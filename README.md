Avatar For Profile Generator
=============

This is a small JavaScript site to create avatars in the style of my own profile image. There are currently 3 facial structures and a few choices in hair, face, clothes and accessories. You can also choose the size of the image you wish to save from full size at 500x500px to 100x100px in 100px increments.  

You may wear one of each item category at a time.

To use the JavaScript file
-----------
The avatar creator is part of a wider project effort to create a template based Dress-Up Game system. Game creators would be able to create folders with specific structural patterns and a single JSON file for defining this structure and other settings.  

To use the generator in it's current state you will need to first decide on a canvas size. The canvas size will determine most of the other choices you have.

Once you have a canvas size you will want to construct your first base (you should have at least one) which fits within your canvas size. While making images please make sure your full size images include the entire canvas, this does not yet have custom positioning so images must be a PNG of the same size and position to the canvas/base.  

You may then either construct the folders immediately or keep a working folder for your items until you are ready. You may discover as you are making your items that you need more folders to layer things as you desire. I have included a template folder in the main directory for use.

~~~~
You may only use letters in your folder name
Items will be named in the format foldername[num]_[letter][word]
~~~~

The JSON format looks like this: (I have included a template folder in the main directory for use.)
~~~~
{
  "canvas": {
    "width": ?00,
    "height": ?00
  },
  "multilayer": {
      "1":[""],
      "2":["", "back"],
      "3":["", "mid", "back"]
    },
  "folders":[
    {"name":"foldername","items":1,"colors":1,"priority":[4,2,1]}
    {"name":"foldername","items":1,"colors":2,"priority":[6,3}
    {"name":"foldername","items":1,"colors":1,"priority":[5]}
  ],
  "colors":[
    {"name":"None", "rgb":[]},
    {"name":"HTMLcolorName", "rgb":[10, 100, 200]},
    {"name":"Black", "rgb":[0, 0 ,0]}
  ]
}
~~~~

The canvas size will match your full image size.
The multilayer section should have the number of layers as the key and an array of words as the value.
The folders section includes the name of your folders, how many items in each folder, the number of colors those items come in, and the priority of each layer for those items.. If you have items 0-10 then you have 10 items as the blank item does not count. Layer priority should be ordered by the first layer being put down to the last (ie. farthest back layer is 1, followed by 2, ect)
While there is an RBG value for colors color selection is currently only supported via HTML color names.  

I have included a template in the template folder.

I have also included a css file with any custom classes found inside the code. All other classes should be built ins from Bootstrap.

###### To DO: ######
- Support RBG color values for custom colors
- Support custom naming schemas for folders (or at least customizable ones)
- Support for changing base changing the items shown to the user
- Generatable HTML templates with ideal layouts for common canvas sizes
