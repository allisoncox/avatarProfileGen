//Initializer, no input
//Output: A promise containing the settings data from JSON file
function getFromSettings(){
  return new Promise(function(resolve, reject) {
    $.ajax({
      dataType: 'json',
      url: 'settings.json',
      success: function(settingsJson) {
        resolve({'Canvas': settingsJson.canvas, 'Multilayer': settingsJson.multilayer, 'Folders': settingsJson.folders, 'bgColors': settingsJson.bgcolors});
      },
      error: function(jXHR, error) {
        reject(error);
      }
    });
  });
}

//Input: The array of folders, the array of layer naming schemas, the array for the color schema
//Output: An array of folders containing a list of items, layers, and colors for each
function createImageArray(settingsFolderArray, multilayerObject, colorSchemeArray) {
  var newFolderArray = [];

  settingsFolderArray.forEach(function(folderObject) {
    var newFolder = {
      'folderName': folderObject.name,
      'multilayer': folderObject.priority.length,
      'itemArray': itemNumberGen(folderObject, multilayerObject, colorSchemeArray)
    };
    newFolderArray.push(newFolder);
  });
  return newFolderArray;
}

//Input: A single folder object, the array of layer naming schemas, the array for the color schema
//Output: An array of items for the specific folder passed in containing a list of layers and colors for each
function itemNumberGen(folderObject, multilayerObject, colorSchemeArray){
  var newItemArray = [];

  for (var itemNum = 0; itemNum < folderObject.items + 1; itemNum++) {
    var newItem = {
      'itemName': folderObject.name + '' + itemNum,
      'itemIcons':createIconFileSources(folderObject, itemNum, colorSchemeArray),
      'itemList': createImageFileSources(folderObject, itemNum, colorSchemeArray, multilayerObject)
    };
    newItemArray.push(newItem);
  }
  return newItemArray;
}

//Input: A single folder object, a single item's number, the array of layer naming schemas, the array for the color schema
//Output: An array of colors for a single item containing a list of layers
function createImageFileSources(folderObject, itemNum, colorSchemeArray, multilayerObject) {
  var newColorsArray = [];

  if(itemNum === 0){
    var newColors = {
      'itemColorName': folderObject.name + itemNum + '_A',
      'layersList': createLayers(folderObject, itemNum, 'A', multilayerObject)
    };
    newColorsArray.push(newColors);
  }else {
    for (var colorId = 0; colorId < folderObject.colors; colorId++) {
      newColors = {
        'itemColorName': folderObject.name + itemNum + '_' + colorSchemeArray[colorId],
        'layersList': createLayers(folderObject, itemNum, colorSchemeArray[colorId], multilayerObject)
      };
      newColorsArray.push(newColors);
    }
  }
  return newColorsArray;
}

//Input: A single folder object, a single item's number, a single color's id, the array of layer naming schemas
//Output: An array of layers for a single item's single color
function createLayers(folderObject, itemNum, colorId, multilayerObject) {
  var newLayerArray = [];

  var multilayerArray = getMultilayerArray(multilayerObject, folderObject.priority.length);

  for (var layerId = 0; layerId < folderObject.priority.length; layerId++) {
    var newlayer = {
      'layerName': folderObject.name + itemNum + '_' + colorId + multilayerArray[layerId],
      'layerPriority': folderObject.priority[layerId],
      'layerSrc': createImageSrc(folderObject.name, itemNum, colorId, multilayerArray[layerId]),
      'layerImage': new Image()
    };
    newLayerArray.push(newlayer);
  }
  return newLayerArray;
}

//Input: The array of layer naming schemas, the number if layers an item has
//Output: The array of name pieces to append to each layer of an item
function getMultilayerArray(multilayerObject, priorityLength){

  var priority = String(priorityLength);
  if(multilayerObject[priority]){
    return multilayerObject[priority];
  }else {
    return errorAlertToUser('Error missing settings for multiple layers. Seek admin. Issue #3');
  }
}

//Input: A single folder object, a single item's number, the array for the color schema
//Output: An array of icons for a single item in a single folder
function createIconFileSources(folderObject, itemNum, colorSchemeArray) {
  var newIconArray = [];

  if(itemNum === 0){
    var newIcon = {
      'iconName': folderObject.name + itemNum + '_A',
      'iconSrc': createImageIconSrc(folderObject.name, itemNum, 'A')
    };
    newIconArray.push(newIcon);
  }else {
    for (var colorId = 0; colorId < folderObject.colors; colorId++) {
      newIcon = {
        'iconName': folderObject.name + itemNum + '_' + colorSchemeArray[colorId],
        'iconSrc': createImageIconSrc(folderObject.name, itemNum, colorSchemeArray[colorId])
      };
      newIconArray.push(newIcon);
    }
  }
  return newIconArray;
}

//Input: A single folder name, a single item number, a single color id, a single layer id
//Output: A source path for an image with the given properties
function createImageSrc(folderName, itemNum, colorId, layerId){
  return 'img/' + folderName + '/' + folderName + itemNum + '_' + colorId + layerId + '.png';
}

//Input: A single folder name, a single item number, a single color id
//Output: A source path for an icon with the given properties
function createImageIconSrc(folderName, itemNum, colorId){
  return 'img/' + folderName + '/icons/' + folderName + itemNum + '_' + colorId + '.png';
}

//Input: The many dimensional array of folders with their associated items, colors and layers
//Output: An array with each item condensed to the parts needed for displaying images on the canvas
function flattenImageArray(folderImgArray){
  var newFlattenedImageArray = [];

  folderImgArray.forEach(function(folderObject) {
    folderObject.itemArray.forEach(function(itemObject) {
      itemObject.itemList.forEach(function(itemColorObject) {
        var item = {
          'folderName': folderObject.folderName,
          'itemName': itemObject.itemName,
          'itemColorName': itemColorObject.itemColorName,
          'layersList': itemColorObject.layersList
        };
        newFlattenedImageArray.push(item);
      });
    });
  });
  return newFlattenedImageArray;
}

//Input: The flattened array of items
//Output: An array of promises for loading each img into the DOM
function loadImagesPromises(flattenedImageArray){
  var newPromisesArray = [];
  flattenedImageArray.forEach(function(itemObject) {
    var layersArray = itemObject.layersList.map(function(layersObject) {
      return new Promise(function(resolve, reject) {
        layersObject.layerImage.onload = function() {
          resolve(layersObject);
        };
        layersObject.layerImage.onerror = function() {
          reject('Rejected' + layersObject.layerImage.src);
        };
        layersObject.layerImage.src = layersObject.layerSrc;
      });
    });
    newPromisesArray = newPromisesArray.concat(layersArray);
  });
  return newPromisesArray;
}

//Input: The flattened array of items
//Output: An array of the default items only
function createResetArray(flattenedImageArray){

  var resetArray = flattenedImageArray.filter(function(element) {return element.itemColorName === element.folderName + '0_A'; });

  return resetArray;
}

//Input: The array of default items
//Output: An array of items used by the user
function initializeUserImageArray(resetImageArray) {
  var newUserImageObjectArray = [];

  resetImageArray.forEach(function(folderObject) {
    var imageNameObject = {
      'folderName': folderObject.folderName,
      'itemName': folderObject.itemName,
      'imageList': folderObject.layersList
    };
    newUserImageObjectArray.push(imageNameObject);
  });
  return newUserImageObjectArray;
}

//Input: The array of all folders and items, an event handler
//Output: Button icons for each item and it's colors
function outputItemIcons(folderImgArray, clickHandlerFunction) {
  for (var folder of folderImgArray) {
    for(var item = 0; item < folder.itemArray.length; item++) {
      createIconSet(folder.itemArray[item].itemIcons, clickHandlerFunction, folder.folderName);
    }
  }
}

//Input: An array of icons for a specific folder, an event handler, a folder name
//Output: Appended icons to the page
function createIconSet(itemIconArray, clickHandlerFunction, folderName){
  var iconSetHTML = '<div class="clearfix marg-pad-0 float-image-set is25">';

  itemIconArray.forEach(function(imageIcon) {
    iconSetHTML += '<button class="btn marg-pad-0 float-images2" type="button" id="' + imageIcon.iconName + '"><img src="' + imageIcon.iconSrc + '"></img></button>';
  });
  iconSetHTML += '</div>';
  var folderId = '#' + folderName;
  $(iconSetHTML).appendTo(folderId);

  addItemIconHandler(itemIconArray, clickHandlerFunction);
}

//Input: An array of icons for a specific folder, an event handler
//Output: Appends handler to button icons
function addItemIconHandler(itemIconArray, clickHandlerFunction) {
  var iconId = '';

  itemIconArray.forEach(function(icon) {
    iconId = '#' + icon.iconName;
    $(iconId).on( 'click', clickHandlerFunction);
  });
}

//Input: Reference to HTML canvas and the settings width/height
//Output: A canvas context we can draw on
function createCanvasContext(canvasElement, canvasWidth, canvasHeight) {
  var context = canvasElement.getContext('2d');
  canvasElement.width = canvasWidth;
  canvasElement.height = canvasHeight;
  return context;
}

//Input: The canvas, canvas context, the array of images for the user, the name of our background color
//Output: Draws selected items onto the canvas display
function drawOutfitImagesToCanvas(canvasElement, canvasContext, userImageArray, bgColorName) {
  var imagePriorityArray = getImagePriorityOnly(userImageArray);
  var sortedUserArray = imagePriorityArray.slice().sort(function(a,b){
    return a.priority - b.priority;
  });

  if(bgColorName === 'None') {
    canvasContext.clearRect(0, 0, canvasElement.width, canvasElement.height);

    sortedUserArray.forEach(function(item) {
      canvasContext.drawImage(item.image, 0, 0);
    });
  } else {
    canvasContext.clearRect(0, 0, canvasElement.width, canvasElement.height);
    canvasContext.fillStyle = bgColorName;
    canvasContext.fillRect(0, 0, canvasElement.width, canvasElement.height);

    sortedUserArray.forEach(function(item) {
      canvasContext.drawImage(item.image, 0, 0);
    });
  }
}

//Input: The user image array
//Output: An array of just image elements and their priorities
function getImagePriorityOnly(userImageArray){
  var imagePriorityArray = [];
  userImageArray.forEach(function(folder) {

    folder.imageList.forEach(function(layer) {
      imagePriorityArray.push({'image': layer.layerImage, 'priority': layer.layerPriority});

    });
  });
  return imagePriorityArray;
}

//Input: Array of colors to pick for background, event handler
//Output: Displays colors to user
function outputColorList(colorArray, clickColorHandlerFunction) {
  for (var color of colorArray) {
    if(color.name === 'None') {
      var colorId = '#None';
      var buttonStart = '<button class="btn marg-pad-0 float-images set-size-30" id="None">';
      var buttonEnd = '</button>';
      var imgElement = '<img class="img-responsive" src="img/item0.png"></img>';
      var button = buttonStart + imgElement + buttonEnd;

      $(button).appendTo('#colors');

      $(colorId).click(clickColorHandlerFunction);
    } else {
      colorId = '#' + color.name;
      button = '<button style="background: ' + color.name + ';" class="btn marg-pad-0 set-size-30 float-images" id="' + color.name + '"></button>';

      $(button).appendTo('#colors');

      $(colorId).click(clickColorHandlerFunction);
    }

  }
}

//Input: The canvas, canvas context, the array of images for the user, the array of flattened items, the name of the item selected, the background color name
//Output: The updated user images array or an error
function changeItemSrc(canvasElement, canvasContext, userImageArray, flattenedImageArray, iconId, bgColorName){
  var folderName = applyRegexMatch(iconId, /[a-zA-Z]+/);

  var userFolderIndex = userImageArray.map(function(element) { return element.folderName; }).indexOf(folderName);
  var item = flattenedImageArray.filter(function(element) {return element.itemColorName === iconId; });

  if(folderName === null || userFolderIndex === -1 || item === -1){
    return errorAlertToUser('Error changing items. Issue #4');
  }

  userImageArray[userFolderIndex].itemName = item[0].folderName;
  userImageArray[userFolderIndex].imageList = item[0].layersList;

  drawOutfitImagesToCanvas(canvasElement, canvasContext, userImageArray, bgColorName);
}

//Input: A string to match and the regex to match with
//Output: The matched item or null if we do not succeed
function applyRegexMatch(stringToMatch, regex) {
  if(stringToMatch.match(regex) !== null) {
    return stringToMatch.match(regex)[0];
  } else {
    return null;
  }
}


//Input: The canvas, canvas context, the array of images for the user, the array for default values, the background color name
//Output: Changes the user array back to the default and displays the new canvas
function resetDoll(canvasElement, canvasContext, userImageArray, resetImageArray, bgColorName) {
  for (var i = 0; i < userImageArray.length; i++){

    userImageArray[i].itemName = resetImageArray[i].itemName;
    userImageArray[i].imageList = resetImageArray[i].layersList;
  }
  drawOutfitImagesToCanvas(canvasElement, canvasContext, userImageArray, bgColorName);
}

//Input: The canvas element
//Output: Places the default png img from the canvas in a modal for the user
function saveDoll(canv, id) {
  var measure = applyRegexMatch(id, /\d+$/);
  var dataURL = canv.toDataURL();
  var dollImage = new Image();

  dollImage.onload = function() {
      dollImage.style.height = measure + 'px';
      dollImage.style.width = measure + 'px';
  };
  dollImage.src = dataURL;

  $('#modalBody').empty();
  $(dollImage).appendTo('#modalBody');
}

//Input: Takes an error message from another function
//Output: Appends the error message to the screen for the user
function errorAlertToUser(alertMessage) {
  var newAlert = '<div class="alert alert-warning alert-dismissible" role="alert"><button type="button" id="modal" class="close" data-dismiss="alert" aria-label="Close" id="CloseAlert"><span aria-hidden="true">&times;</span></button>' + alertMessage + '</div>';

  $(newAlert).prependTo('#DressUp');
}

//Input: None
//Output: Runs the dress-up game
function startDressup() {
  var canvasElement = document.getElementById('canvas');
  var initializeFromSettings = getFromSettings();

  //Input: Return promise for loading JSON
  //Output: Settings made by user as an object or error if not loaded
  initializeFromSettings.then(function(settingsObject){


    var colorSchemeArray = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
    var folderImgArray = createImageArray(settingsObject.Folders, settingsObject.Multilayer, colorSchemeArray);
    var flattenedImageArray = flattenImageArray(folderImgArray);
    var loadImages = loadImagesPromises(flattenedImageArray);

    Promise.all(loadImages).then(function() {


      var resetArray = createResetArray(flattenedImageArray);
      //console.log(resetArray);
      var userImageArray = initializeUserImageArray(resetArray);
      var bgColorsArray = settingsObject.bgColors;

      var bgColorName = 'None';
      var clickHandlerFunction = function(event) {
        var stringId = event.currentTarget.id;
        changeItemSrc(canvasElement, canvasContext, userImageArray, flattenedImageArray, stringId, bgColorName);
      };


      outputItemIcons(folderImgArray, clickHandlerFunction, bgColorsArray);

      var canvasContext = createCanvasContext(canvasElement, settingsObject.Canvas.width, settingsObject.Canvas.height);

      drawOutfitImagesToCanvas(canvasElement, canvasContext, userImageArray, bgColorName);

      var clickHandlerFunction2 = function(event) {
        var stringId = event.currentTarget.id;
        switch(stringId) {
        case 'reset': resetDoll(canvasElement, canvasContext, userImageArray, resetArray, bgColorName);
          break;
        case 'save100':
        case 'save200':
        case 'save300':
        case 'save400':
        case 'save500': saveDoll(canvasElement, stringId);
          break;
        case 'modal':
          break;
        }


      };
      $('#reset').click(clickHandlerFunction2);
      $('#save500').click(clickHandlerFunction2);
      $('#save400').click(clickHandlerFunction2);
      $('#save300').click(clickHandlerFunction2);
      $('#save200').click(clickHandlerFunction2);
      $('#save100').click(clickHandlerFunction2);


      var clickColorHandlerFunction = function(event) {
        var stringId2 = event.currentTarget.id;
        bgColorName = stringId2;
        drawOutfitImagesToCanvas(canvasElement, canvasContext, userImageArray, bgColorName);
      };

      outputColorList(bgColorsArray, clickColorHandlerFunction);

    }).catch(function() {
      errorAlertToUser('Error loading images, missing, seek admin. Issue #2');

    });

  }).catch(function() {
    errorAlertToUser('Failed to load settings. Issue #1');

  });

}

$(document).ready(function() {
  startDressup();
  //$('.do-nicescrol').niceScroll();
});
